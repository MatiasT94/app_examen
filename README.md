# app_examen

Aplicacion para laboratorio infraestructura en la nube

**Clonar repositorio**

$ sudo git clone git@gitlab.com:MatiasT94/app_examen.git

$ cd app_examen/

**Configuracion global del repositorio**

$ sudo git config user.name "istea"
$ sudo git config user.email istea@app-examen.com

**Generar imagen**

$ sudo docker build -t app-examen-image .

**Desplegar contenedores**

$ sudo docker run -d -p 80:80 -v $HOME/app_examen/html:/var/www/html --name app-examen app-examen-image


# Administracion de contenedores

**Comando run**

Ejecutar un contenedor.

$ docker run [imagen]

**Comando ps**

Muestra todos los contenedores que tengan procesos en ejeucion.

$ docker ps

Muestra todos, con procesos en ejecucion y aquellos que no.

$ docker ps -a

**Comando rm**

Eliminar un contenedor. El contenedor no debe tener un proceso en ejecucion.

$ docker rm [container-id|container-name]

**Comando stop**

Detener un contenedor.

$ docker stop [container-id|container-name]

**Comando start**

Iniciar un contenedor.

$ docker start [container-id|container-name]

**Comando restart**

Reiniciar un contenedor.

$ docker restart [container-id|container-name]

**Comando image**

Listar imagenes.

$ docker image ls

**Comando inspect**

Util para ver informacion detallada sobre un contenedor.

$ docker inspect [container-id|container-name]

**Comando exec**

Util para entrar dentro de un contenedor en ejecucion.

$ docker exec -it [container-id|container-name] /bin/bash

**Comando logs**

Util para revisar los logs.

$ docker logs [container-id|container-name]

