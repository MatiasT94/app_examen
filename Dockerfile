FROM ubuntu

RUN apt update && apt install apache2 -y

WORKDIR /home/istea/app_examen

COPY html/ /var/www/html/
COPY apache2.conf /etc/apache2/apache2.conf

EXPOSE 80 443 

CMD ["apache2ctl", "-D", "FOREGROUND"]

