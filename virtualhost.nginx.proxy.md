# Configuración Proxy Reverse

**Instalar Servidor Nginx**

$ sudo apt install nginx

**Virtualhost nginx 'balancer-proxy'**

upstream balancer.proxy.backends {
server 10.0.0.10:80;
server 10.0.0.10:81;
}
server {
listen 83;
server_name balancer.proxy;
location / {
proxy_pass http://balancer.proxy.backends;
proxy_set_header Host $host;
proxy_set_header X-Real-IP $remote_addr;
proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $scheme;
}}

**Activar sitio**

$ sudo ln -s /etc/nginx/sites-available/balancer-proxy /etc/nginx/sites-enabled/

$ sudo nginx -t

$ sudo systemctl reload nginx


